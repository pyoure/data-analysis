'''
program was done by Repalo Anastasiia
file with my own exceptions for program
'''
class NumberOfArgsError(Exception):
    def __str__(self):
        return '\n***** program aborted ***** \n***** command line error *****\n'

class CannotReadError(Exception):
    def __str__(self):
        return '\n***** program aborted ***** \n***** can not read input csv-file *****\n'

class IncorrectCsvError(Exception):
    def __str__(self):
        return '\n***** program aborted ***** \n***** incorrect input csv-file *****\n'


class IncorrectJsonError(Exception):
    def __str__(self):
        return '\n***** program aborted ***** \n***** incorrect input json-file *****\n'

class CannotReadJsonError(Exception):
    def __str__(self):
        return '\n***** program aborted ***** \n***** can not read input json-file *****\n'


class NotEqualError(Exception):
    def __str__(self):
        return '\n***** program aborted ***** \n***** inconsistent information *****\n'

class CannotReadIniError(Exception):
    def __str__(self):
        return '\n***** program aborted ***** \n***** init file error *****\n'

class WrongIniError(Exception):
    def __str__(self):
        return '\n***** program aborted ***** \n***** init file error *****\n'

class OutputtingError(Exception):
    def __str__(self):
        return '\n***** program aborted ***** \n***** can not write output file *****\n'
