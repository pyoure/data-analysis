'''
program was done by Repalo Anastasiia
'''
import json
import pandas as pd
from information import Information

from errorrs import *

def load_stat(help_f, coding):
    '''
    open in the wright encoding and check if the fill of file is ok
    :param help_f: json-file
    :param coding: encoding of the help_f file
    :return: dict from values of the help_f file
    '''
    try:
        with open(help_f, encoding=coding) as f:
            loaded = json.load(f)
            if len(loaded) == 3 and all(isinstance(value, int) for key, value in loaded.items()):
                values_view = loaded.values()
                value_iterator = iter(values_view)
                first_value = next(value_iterator)
                second_value = next(value_iterator)
                third_value = next(value_iterator)
                dict = {'m_total':first_value, 'm_sem':second_value, 'n_stud':third_value}
                return dict
            else:
                raise IncorrectJsonError


    except OSError:
        raise CannotReadJsonError
    except BaseException:
        raise IncorrectJsonError
def load_data(object, main_f, coding):
    '''
    read csv-data from main_f in the wright coding and load it in object
    obj remains clear if there were problems with data-upload
    :param object: object of klas Information
    :param main_f: csv-file
    :param coding: encoding of csv-file

    '''
    object.clear()
    try:
        data = pd.read_csv(main_f, sep=';', encoding=coding, dtype={'mark': 'int64', 'exam': 'int64', 'course': 'object', 'group': 'object', \
                                                                    'semestr': 'int64', 'name2': 'object', 'id': 'int64', 'total': 'int64', 'name1': 'object'})
        object.load_data(data)
    except OSError:
        object.clear()
        raise CannotReadError
    except BaseException:
        object.clear()
        raise IncorrectCsvError


def fit(obj, dict):
    '''
    check if dict-vlaues from help-file are equal to information in object
    '''
    return dict['m_total'] == obj.for_fit_first() and dict['m_sem'] == obj.for_fit_second() and dict['n_stud'] == obj.for_fit_third()

def load(args):
    '''
    create object of klass Information
    print protocol of program work
    '''
    main_f = args[0]
    help_f = args[1]
    coding = args[2]
    object = Information()
    print(f'input-csv {main_f}:', end=' ')
    load_data(object, main_f, coding)
    print('OK')
    print(f'input-json {help_f}:' , end=' ')
    hi = load_stat(help_f, coding)
    print('OK')
    print('json?=csv:', end = ' ')
    if fit(object, hi):
        print('OK')
    else:
        raise NotEqualError
    return object

def outputing(object, args):
    '''
    call object method to output information
    '''
    object.output(args[0].strip(), args[1])











