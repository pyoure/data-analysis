#!/usr/bin/python3
'''
program was done by Repalo Anastasiia
'''
import sys
import json
import klas
from errorrs import *

def info_about_executor():
    return 'These programm was done by Repalo Anastasiia, student of group K-13 Taras Shevchenko National University of Kyiv. Variant 64 \n'
def clause_of_lab():
    return f''' These programm is intended to work up data. In the output-file or to the console will be 
returned information about those students, who have rating higher than mean rating in uploaded data. \n {information_about_useofprogramm()}'''
def information_about_useofprogramm():
    return '''These programm needs two files on input in console. First - the main file of programm and the second - file with additional information. 
Second file should be a dict and contain such keys as input and output. The values of these keys also should be a dicts. 
In input-key value dict has to contain such keys, as 'csv', 'encoding' and 'json'. Corresponding values are files of csv and json format with main and additional information.'''

def load_ini(fname):
    '''
    upload file and check if it is correct
    pre: fname must be json-file
    '''
    try:
        with open(fname) as f:
            a = json.load(f)
            if 'input' in a and 'output' in a and 'csv' in a['input'] and 'json' in a['input'] and 'encoding' in a['input'] and 'encoding' in a['output'] and 'fname' in a['output']:
                return True, a['input']['csv'], a['input']['json'], a['input']['encoding'], a['output']['fname'], a['output']['encoding']
            else:
                return (False,)
    except OSError:
        raise CannotReadIniError
    except BaseException:
        raise CannotReadIniError

def main(fname):
    b = load_ini(fname)
    if b[0]:
        print('OK')
        stat = klas.load(b[1:4])
        klas.outputing(stat, b[4:])
    else:
        raise WrongIniError

print(info_about_executor(), clause_of_lab(), '*****', sep = '\n')
try:
    if len(sys.argv) != 2:
        raise NumberOfArgsError
    else:
        print(f'ini {sys.argv[1]}:', end=' ')
        main(sys.argv[1])


except NumberOfArgsError as e:
    print(e, information_about_useofprogramm())
except CannotReadError as e:
    print(e)
except IncorrectCsvError as e:
    print(e)
except IncorrectJsonError as e:
    print(e)
except CannotReadJsonError as e:
    print(e)
except NotEqualError as e:
    print(e)
except WrongIniError as e:
    print(e, information_about_useofprogramm())
except CannotReadIniError as e:
    print(e)
except OutputtingError as e:
    print(e)
except Exception as e:
    print('\n***** program aborted *****', e)